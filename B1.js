function SapXep(){
    console.log("Bài 1: Sắp xếp 3 số theo thứ tự tăng dần");
    var a = document.getElementById("txtSoThu1_B1").value * 1;
    var b = document.getElementById("txtSoThu2_B1").value * 1;
    var c = document.getElementById("txtSoThu3_B1").value * 1;
    console.log('so thứ 1: ', a);
    console.log('so thứ 2: ', b);
    console.log('so thứ 3: ', c);

    if(a > b && b > c){
        document.getElementById("KQ").innerHTML = 
        `<h2> Thứ tự tăng dần 3 số là: ${c} < ${b} < ${a} </h2>`
        console.log("Thứ tự tăng dần 3 số là:"+"\t"+c+"<"+b+"<"+a);
    }else if(a > b && c > b){
        document.getElementById("KQ").innerHTML = 
        `<h2> Thứ tự tăng dần 3 số là: ${b} < ${c} < ${a} </h2>`
        console.log("Thứ tự tăng dần 3 số là:"+"\t"+b+"<"+c+"<"+a);
    }else if(a > b && c > a){
        document.getElementById("KQ").innerHTML = 
        `<h2> Thứ tự tăng dần 3 số là: ${b} < ${a} < ${c} </h2>`
        console.log("Thứ tự tăng dần 3 số là:"+"\t"+b+"<"+a+"<"+c);
    }else if(b > a && a > c){
        document.getElementById("KQ").innerHTML = 
        `<h2> Thứ tự tăng dần 3 số là: ${c} < ${a} < ${b} </h2>`
        console.log("Thứ tự tăng dần 3 số là:"+"\t"+c+"<"+a+"<"+b);
    }else if(b > c && c > a){
        document.getElementById("KQ").innerHTML = 
        `<h2> Thứ tự tăng dần 3 số là: ${a} < ${c} < ${b} </h2>`
        console.log("Thứ tự tăng dần 3 số là:"+"\t"+a+"<"+c+"<"+b);
    }else{
        document.getElementById("KQ").innerHTML = 
        `<h2> Thứ tự tăng dần 3 số là: ${a} < ${b} < ${c} </h2>`
        console.log("Thứ tự tăng dần 3 số là:"+"\t"+a+"<"+b+"<"+c);
    }

    console.log("---------------------------------------");
}